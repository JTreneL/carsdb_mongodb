import pymongo
import sys


myclient = pymongo.MongoClient("mongodb://localhost:27017/")

mydb = myclient["carsdatabase"]

mycol = mydb["Audis"]

car = [
    {"_id": 1,"Brand": "Audi", "Model": "A5", "Price": 400000},
    {"_id": 2,"Brand": "Audi", "Model": "A7", "Price": 120000},
    {"_id": 3,"Brand": "Audi", "Model": "A5", "Price": 240000},
    {"_id": 4,"Brand": "Audi", "Model": "A8", "Price": 225000},
    {"_id": 5,"Brand": "Audi", "Model": "A1", "Price": 145000},
    {"_id": 6,"Brand": "Audi", "Model": "A6", "Price": 160000},
    {"_id": 7,"Brand": "Audi", "Model": "A5", "Price": 778000},
    {"_id": 8,"Brand": "Audi", "Model": "A5", "Price": 874000},
    {"_id": 9,"Brand": "Audi", "Model": "A5", "Price": 149000},
    {"_id": 10,"Brand": "Audi", "Model": "A5", "Price": 985000}
   ]


def crtmongdb():
    try:
        x = mycol.insert_many(car)
        print("DB Created.")
    except:
        print("DB exist.")


def findall():
    print("ALL RECORDS:")
    findol = mycol.find().sort("_id", +1)
    for x in findol:
        print(x) 


def findbyid():
    ident = input("Enter Id: ")
    try:
        s = int(ident)
        myid = { "_id":s}
        usrsearch = mycol.find(myid)
        for x in usrsearch:
            print(x) 
    except:
        print("This is not number!")


def deletemany():
    x = mycol.delete_many({})
    print("DB DELETED.")


def deletebyid():
    delid = int(input("Enter Id: "))
    myid = { "_id":delid}
    x = mycol.delete_one(myid)
    print(f"CAR WITH ID: {delid}, REMOVED.")


def getidmongo():  
    myresult = mycol.find().sort("_id", -1).limit(1)
    for x in myresult:
        x = list(x.values())
        x = (x[0])
        x = x + 1
        return x
        

def insertcar():
    getbrand = input("Enter brand: ")
    getmodel = input("Enter model: ")
    getprice = input("Enter price: ")
    getid = getidmongo()
    userinsert = {"_id": getid,"Brand": getbrand, "Model": getmodel, "Price": getprice}
    x = mycol.insert_one(userinsert)
    print("Car added:")


def main():
    while True:
        menu1 = input("1: CRT MONGODB 2: FINDALL 3: FIND BY ID 4. DELETE ALL 5. DELETE BY ID 6. INSERT CAR 7. EXIT ")
        if menu1 == "1":
            crtmongdb()
        elif menu1 == "2":
            findall()
        elif menu1 == "3":
            findbyid()
        elif menu1 == "4":
            deletemany()
        elif menu1 == "5":
            deletebyid()
        elif menu1 == "6":
            insertcar()
        elif menu1 == "7":
            break
        else:
            print("WRONG INPUT")

    
if __name__ == "__main__":
   sys.exit(main())



