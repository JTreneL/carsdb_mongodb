# Cars database:
- Database with cars in MongoDB
- Can insert car 
- Can delete car by id
- Can find car by id

# Install

- extract all files to new directory
- open console and go to directory
- enter ./install.sh


# Uninstall

- open console and go to directory
- enter ./uninstall.sh


# How to Run it:

- open console
- enter [username@fedora ~]$ carsdb


# License:

- opensource
