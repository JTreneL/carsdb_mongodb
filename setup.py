from setuptools import setup, find_packages
import pathlib
from setuptools.command.install_scripts import install_scripts
import subprocess
from distutils.util import convert_path
import os
import subprocess

here = pathlib.Path(__file__).parent.resolve()
path_ = subprocess.run("pwd", capture_output=True, shell=True).stdout.decode().strip() + "/"
with open(os.path.join(path_, 'carsdb/data/VERSION')) as version_file:
   __version__ = version_file.read().strip()


setup(
    name='carsdbmongo',  # Required
    version=__version__,
    url='https://gitlab.com/JTreneL/dbtest',  # Optional
    author='JTrenel',  # Optional
    author_email='Janlenert@email.cz',  # Optional
    packages=find_packages(),  # Required
    package_data={'carsdb': [
        "data/VERSION",
        ]},
    install_requires=[
        'pymongo',
        ],
    entry_points={"console_scripts": ["carsdb = carsdb.carsdbmongo:main"]},
)

